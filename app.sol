pragma solidity ^0.5.11;
pragma experimental ABIEncoderV2;

import "./typos.sol";

contract App {

	Typos.Record[] internal progress;
	Typos.Terms config;

	constructor(int a, int b) public {
		config.TaxA = a;
		config.TaxB = b;
	}

	function Log(Typos.Record memory r) public {
		progress.push(r);
	}

	function Retrieve() public view returns (Typos.Record[] memory log) {
		return progress;
	}
}
