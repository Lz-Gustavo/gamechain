pragma solidity ^0.5.11;
pragma experimental ABIEncoderV2;

import "./typos.sol";
import "./app.sol";

contract History {

	address public ourWallet;
	mapping(address => App) internal state;

	function Subscribe(address ad, Typos.Terms memory t) public {
		state[ad] = new App(t.TaxA, t.TaxB);
	}

	function Unsubscribe(address ad) public {
		delete state[ad];
	}

	function Log(address ad, string memory data) public {
		Typos.Record memory r = Typos.Record(block.timestamp, data);
		state[ad].Log(r);
	}

	function Retrieve(address ad) public view returns (Typos.Record[] memory log) {
		return state[ad].Retrieve();
	}
}